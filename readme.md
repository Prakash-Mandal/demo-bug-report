## Demo - JavaScript Project on maintaing local storage

### Bug Report Management

This demo proect includes CRUD operations on Bug Objects which consist of :
* **Id** : To represent id of the Bug Report
* **Title** : To represent the title of the report with which we can see the report
* **Description** : To describe the actual problem or error bug you are facing
* **Severity** : The level of bug which is affect the working of the application
* **Reported By** : The person who is adding the bug
* **Owner** : The referencing person to which the bug is assigned to solve
* **Reported Date** : The date and time in which the bug was reported

There is a Add button on the top of teh application for adding or reporting a new bug.
And then we have the list of teh curent bugs present in the database. And it is also provided with delete and update options which can be used to delet ethe bug after resolving of the Bug. And updating the report if anything was mistakenly left during creation of the bug.