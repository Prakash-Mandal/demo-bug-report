
function Bug (Id, Title, Description, Severity, ReportedBy, Owner, ReportedDateTime, ModifiedDate) {
    this.Id = Id;
    this.Title = Title ;
    this.Description  = Description;
    this.Severity = Severity;
    this.ReportedBy = ReportedBy;
    this.Owner = Owner;
    this.ReportedDate = ReportedDateTime;
    this.ModifiedDate = ModifiedDate;
}

function checkBug(title) {
    console.log("checking : "+title);
    let data = JSON.parse(window.localStorage.getItem("BUG"));
    for (let x in data) {
        if (x.hasOwnProperty("Title")) {
            return (title === data[x]["Title"]);
        }
    }

}

function addBug() {

    let bugs = [];
    let i = 0;
    let data = JSON.parse(window.localStorage.getItem("BUG"));

    let title = document.getElementById("Title").value;
    title = escapeHtml(title);
    let desc = document.getElementById("Desc").value;
    desc = escapeHtml(desc);
    let reportby = document.getElementById("ReportByList").value;
    let owner = document.getElementById("OwnerList").value;
    let severityType = severity();

    if(null !== data) {
        for (let x in data["records"]) {
            console.log(data["records"][x]);
            bugs.push(data["records"][x]);
            i++;
        }
    }

    let reportdate = new Date().toString();
    let modifieddate = new Date().toString();

    let bug = new Bug(i+1,title, desc, severityType, reportby, owner, reportdate, modifieddate);

    bugs.push(bug);
    saveBug(bugs);
    location.reload();
}

function deleteBug(i) {
    let bugs = [];
    let data = window.localStorage.getItem("BUG");
    data = JSON.parse(data);

    for (let x in data["records"]) {
        console.log("id's : "+data["records"][x].Id)
        if(i.toString() !== data["records"][x].Id.toString()) {
            let temp = new Bug(
                data["records"][x].Id,
                data["records"][x].Title,
                data["records"][x].Description,
                data["records"][x].Severity,
                data["records"][x].ReportedBy,
                data["records"][x].Owner,
                data["records"][x].ReportedDate,
                data["records"][x].ModifiedDate
            );
            bugs.push(temp);
        }
    }
    saveBug(bugs);
    location.reload();
}

function updateBug() {
    let id = document.getElementById("UpdateId").value;
    let title = document.getElementById("UpdateTitle").value;
    let desc = document.getElementById("UpdateDesc").value;
    let reportby = document.getElementById("UpdateReportByList").value;
    let owner = document.getElementById("UpdateOwnerList").value;
    let severity = document.getElementById("UpdateSeverity").value;
    let date = document.getElementById("ReportedDate").value.toString();

    let bug = new Bug(
        id,
        title,
        desc,
        severity,
        reportby,
        owner,
        date,
        new Date().toString());

    let data = window.localStorage.getItem("BUG");
    data = JSON.parse(data);
    let bugs = [];
    for (let x in data["records"]) {
        bugs.push(data["records"][x]);
    }
    console.log(bugs);
    bugs[id-1]=bug;
    console.log(bugs);
    saveBug(bugs);
    location.reload();
}

function showBugs() {
    let show = document.getElementById("BugData");
    let bugdata = window.localStorage.getItem("BUG");
    bugdata = JSON.parse(bugdata);

    let i = bugdata["records"].length;
    if (0 === i) {
        show.innerHTML = "No data";
        return false;
    }

    let table = document.createElement("Table");
    table.setAttribute("id", "TableBug");
    table.setAttribute("class", 
        "table  table-condensed table-hover ");
    show.appendChild(table);

    let tr = document.createElement("tr");
    for(let x in bugdata["records"]){
        for(let y in bugdata["records"][x]){
            let th = document.createElement("th");
            let text = document.createTextNode(y);
            th.appendChild(text);
            tr.appendChild(th);            
        }
        break;
    }
    table.appendChild(tr);


    let body = document.createElement("tbody");
    table.appendChild(body);
    for(let x in bugdata["records"]) {
        let tr = document.createElement("tr");
        let i = bugdata["records"][x].Id.toString();

        let updatebtn = document.createElement("button");
        updatebtn.setAttribute("id",i)
        updatebtn.setAttribute("class", "btn btn-info");
        updatebtn.setAttribute("type", "submit");
        updatebtn.setAttribute("onclick","UpdateBug("+i+")");
        updatebtn.setAttribute("data-toggle", "modal");
        updatebtn.setAttribute("data-target", "#UpdateNewBug");
        updatebtn.innerHTML = "Update";

        let deletebtn = document.createElement("button");
        deletebtn.setAttribute("class", "btn btn-danger");
        deletebtn.setAttribute("type", "submit");
        deletebtn.setAttribute("onclick","return promptDelete("+i+")");
        deletebtn.innerHTML = "Delete";

        for (let y in bugdata["records"][x]) {
            if ( "Id" === y.toString()) {
                let th = document.createElement("th");
                let text = document.createTextNode(bugdata["records"][x][y]);
                th.appendChild(text);
                tr.appendChild(th);
            } else {
                let td = document.createElement('td');
                let text = document.createTextNode(bugdata["records"][x][y]);
                td.appendChild(text);
                tr.appendChild(td);
            }
        }

        let td = document.createElement('td');
        td.appendChild(deletebtn);
        tr.appendChild(td);
        td = document.createElement('td');
        td.appendChild(updatebtn);
        
        tr.appendChild(td);
        body.appendChild(tr);
    }
}

function saveBug(bugs) {
    let obj = {"records" : bugs};
    let data = JSON.stringify(obj);
    window.localStorage.setItem("BUG", data);
    console.log("Bug Report Updated");
}


