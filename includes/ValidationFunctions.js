function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

function myDateFormat() {
    let textDate = "";
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var d = new Date();
    textDate += d.getDate()  + "," + months[d.getMonth()+1] + " " + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() ;
}

function promptDelete(i) {
    if (confirm("Sure to delete the Bug Report!")) {
        deleteBug(i);
        return true;
    } else {
        return false;
    }
}

function promptUpdate(i) {
    if (confirm("Sure to update the Bug Report!")) {
        updateBug(i);
        return true;
    } else {
        return false;
    }
}
function severity() {
    if (document.getElementById("critical").checked) {
        return "Critical";
    } else if (document.getElementById("major").checked) {
        return "Major";
    } else if (document.getElementById("minor").checked) {
        return "Minor";
    } else if (document.getElementById("trivial").checked) {
        return "Trivial";
    }
}

function reportBug() {
    let title = document.getElementById("Title").value;
    let desc = document.getElementById("Desc").value;
    desc = escapeHtml(desc);
    let reportby = document.getElementById("ReportByList").value;
    let owner = document.getElementById("OwnerList").value;
    let severityType = severity();
    if(title && desc && reportby && owner && severity) {
        addBug(title, desc, severityType, reportby, owner);
    } else {
        console.log("Cannot Add report");
    }
}

function UpdateBug(i) {
    let data = window.localStorage.getItem("BUG");
    data = JSON.parse(data);
    for (let x in data["records"]) {
        if(i == data["records"][x].Id) {
            document.getElementById("UpdateId").value = data["records"][x].Id;
            document.getElementById("UpdateTitle").value = data["records"][x].Title;
            document.getElementById("UpdateDesc").value = data["records"][x].Description;
            document.getElementById("UpdateReportByList").value = data["records"][x].ReportedBy;
            document.getElementById("UpdateOwnerList").value = data["records"][x].Owner;
            document.getElementById("UpdateSeverity").value = data["records"][x].Severity;
            document.getElementById("ReportedDate").value = data["records"][x].ReportedDate;
        }
    }
}
